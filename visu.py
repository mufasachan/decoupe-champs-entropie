import numpy as np
from skimage import io
import matplotlib.pyplot as plt

#   Paramètre de la fenêtre de visionnage
x_min = 750
x_max = 1500
y_min = 400
y_max = 5000
est_affiche_entier = 1

#   Extraction du bout de l'image que nous souhaitons
img = io.imread('./data/_10mBands_20180927.tif')
if (est_affiche_entier):
    img = img[:,:,:3]
else:
    img = img[x_min:x_max,y_min:y_max,:3]
img = (img / np.max(img) * 255).astype(int)

#   Affichage
io.imshow(img, vmin=0, vmax=1)
plt.show()