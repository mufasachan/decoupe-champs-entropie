"""
    Ce programme permet de caler le masque en tattonnant en partant des estimations papiers déjà faite précédemment. Il
    faudra ensuite faire toutes cette opération dans une fonction pour directement faire l'opération de masquage.
    On applique le masque à l'entropie locale calculé sur l'ensemble de l'image. Puis nous appliquons le filtre à
    l'entropie locale.
"""

from skimage import io

#   Fonctions écrites dans ce programme
from functions.detect_champ import *
from functions.img_Import import *
from functions.appli_mask import *
from functions.entropyL import *
from functions.img_seuil import *

from skimage import filters

import numpy as np
import matplotlib.pyplot as plt
import cv2

#   Chargement des images
img = img_Import('./data/_10mBands_20180927.tif','./data/_20mBands_20180927.tif') # Hyperspectrale
map = cv2.imread('./data/champs.jpg')

#   Paramètre de découpe de l'image
#   On divise tout par deux car on downscale
map_l = 1031+22-26   # Compris dans [1031 ; 1037] après estimation papier
map_h = 368 # Égale à 368 après estimation papier
map_start_point_x = 3251-19+17+1 # D'après estimation papier
map_start_point_y =  5570-11*2 #5572 # D'après estimation papier
map_stop_point_x = map_start_point_x + map_l # D'après estimation papier
map_stop_point_y = map_start_point_y + map_h # D'après estimation papier

#   Formatage des données
img = img[map_start_point_y:map_stop_point_y,map_start_point_x:map_stop_point_x,:]
img = 255 * (img / np.max(img))
img = img.astype(int)
map = cv2.resize(map, (map_l,map_h))
imgRGB = img[:,:,:3]
imgNew = np.zeros(imgRGB.shape)
for i in range(1, len(imgNew) - 1):
    for j in range(1, len(imgNew[i]) - 1):
        m = np.sum(map[i,j,:])/3
        if m > map[i,j,0]-3 and m < map[i,j,0]+3:
            imgNew[i,j,:] = imgRGB[i,j,:]
        else:
            imgNew[i, j, :] = map[i, j, :]
plt.figure()
plt.imshow(imgNew.astype(int))
plt.show()
plt.figure()
plt.imshow(imgRGB)
plt.show()
#   Calcul de l'entropie de l'image
eL = calcul_entropie(img)

#   Détéction champs sur la map
mask = detect_champ(map)
mask = filters.median(mask,disk(1))
# img_m = np.zeros(img.shape)
# for i in range(len(img_m)):
#     for j in range(len(img_m[i])):
#         if mask[i,j]:
#             img_m[i,j] = img[i,j]
#         else:
#             img_m[i,j] = np.zeros(10)
# img_m_rgb = img_m[:,:,:3]
# plt.imshow(img_m_rgb / np.max(img_m_rgb))
#plt.plot()
#   Opération de masquage
eL_m = appli_mask(eL,mask)

#   Seuillage
#eL = img_seuil(eL, [0.1,0.5])

#   Affichage du résultat
plt.imshow(eL_m)
plt.colorbar()
plt.show()

plt.plot()
plt.hist(eL_m.ravel(),64,[0.1,np.max(eL_m)])
plt.show()