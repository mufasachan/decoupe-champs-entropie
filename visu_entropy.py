from functions.img_Import import *
from functions.entropyL import calcul_entropie

from skimage import io
import matplotlib.pyplot as plt

#   Paramètre de la fenêtre de visionnage
x_min = 750
x_max = 1500
y_min = 400
y_max = 5000
est_affiche_entier = 1

#   Extraction de l'image
img = img_Import('./data/_10mBands_20180927.tif','./data/_20mBands_20180927.tif')
if (not(est_affiche_entier)):
    img = img[x_min:x_max,y_min:y_max,:]

#   Calcul de l'entropie
eL = calcul_entropie(img)

# Affichage de l'entropie locale
io.imshow(eL)
plt.show()