import numpy as np
import cv2
from skimage.io import imread

def img_Import(path_10,path_20):
    """
    Cette fonction permet de donner l'image en entier en faisant du upscale (par interpolation bilinéaire)
    :param path_10: Le  chemin d'accès à l'image en 10m
    :param path_20: Le chemain d'accès à l'image en 20m
    :return img: L'image de sortie avec l'ensemble des bandes en grand format
    """
    # Chargement des images
    img_10 = imread(path_10)
    img_20 = imread(path_20)

    #   Initialisation des variables
    Nbande = img_20.shape[2]
    nouvelle_shape = img_10.shape[:2]

    #   Downscale de l'image avec une interpolation bilinéaire
    img_up = cv2.resize(img_20, (nouvelle_shape[1],nouvelle_shape[0]))

    #   On peut concatener les deux images
    img = np.concatenate((img_10,img_up), axis=2)
    return img
