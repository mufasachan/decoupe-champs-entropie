import numpy as np

def appli_mask(eL, mask):
    """
    Cette fonction applique le masque binaire à l'image
    :param eL: L'entropie locale, variable 2-D, float, (Nx,Ny)
    :param mask:  Le masque à appliquer, variable 2-D, bool (Nx,Ny)
    :return el_mask: L'entropie locale masquée oh hé oh hé (Nx,Ny)
    """
    #   Variables de travail
    eL_mask = np.zeros(eL.shape)  # L'image masqué oh hé oh hé

    #   Application du masque
    for i in range(len(mask)):
        for j in range(len(mask[i])):
            if mask[i,j]: # Couleurs
                eL_mask[i,j] = eL[i,j]
            else: # Gris
                eL_mask[i,j] = 0

    #   OP terminé
    return eL_mask
