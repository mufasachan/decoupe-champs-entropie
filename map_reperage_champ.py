from functions.img_Import import *
from functions.entropyL import *
from functions.detect_champ import *
from functions.appli_mask import *

from skimage.filters import median
from skimage.morphology import disk
from skimage.io import imread
from cv2 import resize

import numpy as np
import matplotlib.pyplot as plt

#   Fenêtre de calcul
x_start = 529
y_start = 5264
win_l = 278
win_h = 258

#   Image de travail
img = img_Import('./data/_10mBands_20180927.tif','./data/_20mBands_20180927.tif')
img = img[y_start:y_start + win_h, x_start:x_start + win_l, :]
img = (255 * img / np.max(img)).astype(int)

#   Database Champs
champs = imread('./data/db_champs.png')[:,:,:3]
champs = resize(champs, (win_l,win_h))
mask = detect_champ(champs)

#   Pour caler le masque
# imgRGB = img[:,:,:3]
# img_new = np.zeros(imgRGB.shape)
# for i in range(len(img_new)):
#     for j in range(len(img_new[i])):
#         if mask[i,j]:
#             img_new[i,j,:] = champs[i,j,:3]
#         else:
#             img_new[i,j,:] = imgRGB[i,j,:]
# plt.figure()
# plt.imshow(img_new.astype(int))
# plt.figure()
# plt.imshow(imgRGB.astype(int))
# plt.show()

#   Calcul d'entropie
eL = calcul_entropie(img)
eL = ( 255 * eL / np.max(eL)).astype(int)
eL_m = appli_mask(eL,mask)
eL_m_pre = eL_m

#   Opération de seuillage
seuil = 150
for i in range(len(eL_m)):
    for j in range(len(eL_m[i])):
        if eL_m[i,j] >= seuil or eL_m[i,j] == 0:
            eL_m[i,j] = 0
eL_m = eL_m / np.max(eL_m)

#   Filtre médian
taille_disk = 5
eL_m = median(eL_m, disk(taille_disk))

#   Superposition
img_super = np.zeros(img[:,:,:3].shape,dtype=int)
for i in range(len(img_super)):
    for j in range(len(img_super[i])):
        if eL_m[i,j]:
            img_super[i,j] = (eL_m[i,j] * np.ones(3)).astype(int)
        else:
            img_super[i,j] = img[i,j,:3].astype(int)

#   Affichage
plt.figure()
plt.imshow(img_super)
# plt.imshow(eL_m_pre)
# plt.figure()
# plt.imshow(eL_m)
plt.show()
